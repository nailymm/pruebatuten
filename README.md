## Question2 API
Desarrollo de un servicio API REST para obtener la hora en formato UTC de 2 parámetros enviados al servicio: hora y timezone, usando Spring Boot.

## Especificaciones tecnicas
Java 11
Spring boot 2.5.2
Maven 4.0

## Setup:
Clonar repositorio.
`$ git@gitlab.com:nailymm/pruebatuten.git`

Hacer el build con maven.
`$ mvn clean package`

Ejecutar con el tomcat embebido de spring boot.
`$ mvn spring-boot:run`

La aplicacion queda disponible en: http://localhost:8080

Para las configuraciones pertinentes, dirigirse al application.properties

## Mejoras Futuras
- Aplicación de Tests Unitarios

