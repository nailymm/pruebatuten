/**
 * 
 */
package com.time.entity;

public class TimeResponse {
	String time;
	String timezone;
	
	public TimeResponse(String time, String timezone) {
		super();
		this.time = time;
		this.timezone = timezone;
	}
	
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	
}
