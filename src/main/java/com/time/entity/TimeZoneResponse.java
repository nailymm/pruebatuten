/**
 * 
 */
package com.time.entity;

/**
 * @author naily
 *
 */
public class TimeZoneResponse {
	private String gmt;
	private String zone;
	private String fullName;
	
	public TimeZoneResponse(String gmt, String zone) {
		super();
		this.gmt = gmt;
		this.zone = zone;
		this.fullName = String.format("(%s%s) %s","GMT", gmt, zone);
	}
	public String getGmt() {
		return gmt;
	}
	public void setGmt(String gmt) {
		this.gmt = gmt;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getFullName() {
		return this.fullName;
	}
	public void setFullName() {
		this.fullName = String.format("(%s%s) %s","GMT", gmt, zone);
	}
	@Override
	public String toString() {
		return String.format("(%s%s) %s","GMT", gmt, zone);
	}

}
