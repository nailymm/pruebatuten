/**
 * 
 */
package com.time.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.time.entity.TimeResponse;
import com.time.entity.TimeZoneResponse;
import com.time.services.TimeService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class TimeController {
	
	private TimeService timeService = new TimeService();
	
	@PostMapping("/convertTime")
	public TimeResponse convertTime(@RequestParam("time")String time, @RequestParam("zone")String timeZone){
		return timeService.getTime(time, timeZone);
	}
	
	@GetMapping("/allTimeZones")
	public List<TimeZoneResponse> getTimeZones(){
		return timeService.getTimeZones();
	}
}
