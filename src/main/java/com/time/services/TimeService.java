/**
 * 
 */
package com.time.services;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import com.time.entity.TimeResponse;
import com.time.entity.TimeZoneResponse;

public class TimeService {
	public TimeResponse getTime(String time, String timeZone) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		System.out.println(sdf.format(calendar.getTime()));    
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		TimeResponse response = new TimeResponse(sdf.format(calendar.getTime()), "utc");
		return response;
	}
	private String getOffset(LocalDateTime dateTime, ZoneId id) {
	    return dateTime
	      .atZone(id)
	      .getOffset()
	      .getId()
	      .replace("Z", "+00:00");
	}
	
	public List<TimeZoneResponse> getTimeZones() {
		Set<String> zones = ZoneId.getAvailableZoneIds();
		List<TimeZoneResponse> responses = new ArrayList<TimeZoneResponse>();
		LocalDateTime now = LocalDateTime.now();
		for (Iterator<String> iterator = zones.iterator(); iterator.hasNext();) {
			String value = (String) iterator.next();
			String gmt = getOffset(now, ZoneId.of(value));
			TimeZoneResponse obj = new TimeZoneResponse(gmt, value);
			responses.add(obj);
		}
		return responses;
	}
	
}
